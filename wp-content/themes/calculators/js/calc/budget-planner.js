(function ($) {
  var $tabContainers = $('.tab-container');
  var $pageNumber = $('#pageNumber');
  var $nav = $('.nav-menu');
  var $navTabs = $nav.children('li');
  var $summaryCategories = $('.summary-category');
  var $tabsArrow = $('.prev-tab, .next-tab');
  var categoriesTotalSum = {};
  var currentPage = 1;
  var customerResult = [];
  var subResult = [];
  var finalResult = [];
  var resultsCategory = [
    'Income Details',
    'Motor Vehicle / Transport',
	'Property',
    'Living Expenses',
    'Insurance',
    'Loans, Credit Card and Store Accounts',    
    'Entertainment'
  ];
  var budgetCalculator = null;
	
  //$("#reset-fields-modal").modal();

  /**
   * @param value
   * @param frequency
   * @returns {number}
   */
  function calcIncome (value, frequency) {
    frequency = [1, MONTHS_PER_YEAR, FORTNIGHTS_PER_YEAR, WEEKS_PER_YEAR][frequency];
    return value * frequency;
  }

  /**
   * @param {string} category
   * @returns {number}
   */
  function calCategoryTotal (category) {
    var results = 0;
    $.each(categoriesTotalSum['c' + category], function (index, data) {
      results += data.income;
    });
    return results;
  }

  function calcTotals () {
    var i = 0;

    var expenses = {
      weekly: 0,
      monthly: 0,
      income: 0
    };

    var summary = {
      weekly: 0,
      monthly: 0,
      income: 0
    };

    finalResult = [];
	
	finalResult.push({key: "CUSTOMER"});
	for (const [index, object] of Object.entries(customerResult)) {
	  finalResult.push({key: object.key, value: object.value});
	}
	finalResult.push({key: "SEPARATOR"});
	
	finalResult.push({key: "ALL ITEMS"});
	finalResult.push({key: "SEPARATOR"});
	
	for (const [index, object] of Object.entries(subResult)) {
	  finalResult.push({key: object.key, value: object.value, id: object.id});
	}
	
	finalResult.push({key: "SEPARATOR"});
	finalResult.push({key: "SECTIONS"});
	finalResult.push({key: "SEPARATOR"});

    $.each(categoriesTotalSum, function (index, cols) {		
      var $categoryRow = $summaryCategories.eq(i);
      var $cols = $categoryRow.find('div');
      var colIndex = 0;
      var totals = {
        weekly: 0,
        monthly: 0,
        income: 0
      };

      $.each(cols, function (index, data) {
        totals.weekly += data.weekly;
        totals.monthly += data.monthly;
        totals.income += data.income;
      });

      $.each(totals, function (field, value) {
        if (i === 0) {
          summary[field] += value;
        } else if (index != 'c6') {
          expenses[field] += value;
        }
        $cols.eq(colIndex).find('p').text(LoanCalc.addCommas(value));
        colIndex++;
      });
	  
      finalResult.push({key: resultsCategory[i], value: ''});
      finalResult.push({key: 'Weekly', value: '$' + LoanCalc.addCommas(totals.weekly)});
      finalResult.push({key: 'Monthly', value: '$' + LoanCalc.addCommas(totals.monthly)});
      finalResult.push({key: 'Annual', value: '$' + LoanCalc.addCommas(totals.income) });
      finalResult.push({key: "SEPARATOR"});

      i++;
    });

    $.each(['Total Expenses', 'SURPLUS'], function (index, label) {
      var data
        , fieldsKey
      ;

      if (index === 0) {
        data = $.extend({}, expenses);
        fieldsKey = 'expenses';
      } else {
        data = $.extend({}, summary);
        fieldsKey = 'surplus';

        if (expenses.monthly > data.monthly || expenses.weekly > data.weekly || expenses.income > data.income) {
          label = 'DEFICIT';
		  $('#income-lesser-than-expenses').show();
		  $('#income-equal-expenses').hide();
		  $('#income-greater-than-expenses').hide();
		} else if (Math.abs(expenses.income - data.income) == 0) {
		  $('#income-lesser-than-expenses').hide();
		  $('#income-equal-expenses').show();
		  $('#income-greater-than-expenses').hide();
        } else {
		  $('#income-lesser-than-expenses').hide();
		  $('#income-equal-expenses').hide();
		  $('#income-greater-than-expenses').show();
		}

        data.monthly = Math.abs(expenses.monthly - data.monthly);
        data.weekly = Math.abs(expenses.weekly - data.weekly);
        data.income = Math.abs(expenses.income - data.income);
        $('#surplus-label').text(label + ':');
      }

      $('#' + fieldsKey + '-weekly-value').text(LoanCalc.addCommas(data.weekly));
      $('#' + fieldsKey + '-monthly-value').text(LoanCalc.addCommas(data.monthly));
      $('#' + fieldsKey + '-income-value').text(LoanCalc.addCommas(data.income));

      finalResult.push({key: label, value: ''});
      finalResult.push({key: 'Weekly', value: '$' + LoanCalc.addCommas(data.weekly)});
      finalResult.push({key: 'Monthly', value: '$' + LoanCalc.addCommas(data.monthly)});
      finalResult.push({key: 'Annual', value: '$' + LoanCalc.addCommas(data.income)});
      finalResult.push({key: "SEPARATOR"});
    });
  }

  /**
   * @param {jQuery.Event} e
   */
  function onFieldUpdate (e) {
    var $row = $(this).closest('li');
    var $category = $row.closest('[data-category]');
    var category = $category.data('category');
	var label = $($row.find('label')[0]).text();
	
	if($row.find('.other').length && $row.find('.other').val().length > 0)
		label = $row.find('.other').val();

    if (category) {
      var frequency;
	  var frequencyText;
      var value;
      var income;
      var total = 0;
	  var typeId;

      if ('select' === this.tagName.toLowerCase()) {
        frequency = this.selectedIndex;
		frequencyText = $(this).val();
        value = $row.find('input[type=text]').val();
		typeId =  $row.find('input[type=text]').data('type-id');
      } else {
        frequency = $row.find('select').get(0).selectedIndex;
		frequencyText = $($row.find('select').get(0)).val();
        value = $(this).val();
        typeId =  $(this).data('type-id');
		$(this).val(LoanCalc.addCommas(value));		
      }
     
	  value = parseFloat(value.replace(',', ''));
	  
	  if (isNaN(value)) {
		value = 0;
	  }
	  
	  var index = subResult.findIndex(x => x.key === label);
	  
	  if (index > -1) {		  
		subResult.splice(index, 2);		  	  		  
	  }

	  if(value > 0) {		  
		subResult.push({key: label, value: '', id: ''});	
		subResult.push({key: frequencyText, value: '$' + LoanCalc.addCommas(value), id: typeId});			
	  }
	  
      income = calcIncome(value, frequency);

      if (!categoriesTotalSum.hasOwnProperty('c' + category)) {
        categoriesTotalSum['c' + category] = {};
      }

      categoriesTotalSum['c' + category][$row.index()] = {
        income: income,
        amount: value,
        weekly: income > 0 ? Math.round(income / WEEKS_PER_YEAR) : 0,
        monthly: income > 0 ? Math.round(income / MONTHS_PER_YEAR) : 0
      };

      $row.find('.amount').text(LoanCalc.addCommas(income));
      $category.find('.total strong').text(LoanCalc.addCommas(calCategoryTotal(category)));
    }

    calcTotals();
  }
  
  /**
   * @param {jQuery.Event} e
   */
  function onCreditCardFieldUpdate (e) {
    var $row = $(this).closest('li');	
	var $label = $(this).siblings().closest('label');
    var $category = $row.closest('[data-category]');
    var category = $category.data('category');
	var label = $($row.find('label')[0]).text();
	
    if (category) {
      var value;
      var income;
      var total = 0;

      value = $(this).val();
	  label += ' ' + $label.text();	  
	  
	  var index = subResult.findIndex(x => x.key === label);
	  
	  if (index > -1) {		  
		subResult.splice(index, 1);		  	  		  
	  }

	  if(value.length > 0) {		  
		if(label.indexOf('Limit') !== -1 || label.indexOf('Balance') !== -1) {
		  $(this).val(LoanCalc.addCommas(value));
		  subResult.push({key: label, value: LoanCalc.addCommas(value)});
		} else {
		  subResult.push({key: label, value: value});	
		}
	  }
    }
	
	calcTotals();
  }

  /**
   * @param {number=} page
   */
  function renderTabs(page) {
    var total = $navTabs.length;
    var tabs = [];
    var baseTabs;

    if ('number' !== typeof page || isNaN(page) || page < 1 || total < (page - 1)) {
      page = $nav.find('.nav-menu-activ');
      page = page.length ? parseInt(page.data('nav'), 10) : 1;
    }

    for (var i=0; i<total; i++) {
      tabs.push($navTabs[i]);
    }

    baseTabs = tabs.splice(page > 3 ? 0 : 3, 3);
    currentPage = page;

    $tabsArrow.removeClass('no-activ');
    if (currentPage === 1) {
      $tabsArrow.eq(0).addClass('no-activ');
    } else if (currentPage === 6) {
      $tabsArrow.eq(-1).addClass('no-activ');
    }

    $pageNumber.text(page);
    $tabContainers.eq(page-1).addClass('active-tab').siblings().removeClass('active-tab');
    //$.fn.append.apply($nav.empty(), tabs.concat(baseTabs));
    $nav.find('[data-nav="' + page + '"]').addClass('nav-menu-activ').siblings().removeClass('nav-menu-activ')

    /*if(page == 1) {
      $('.embed-calculator').height('1850px');
    } else if (page == 2) {
      $('.embed-calculator').height('2300px');
    } else if (page == 3) {
      $('.embed-calculator').height('1500px');
    } else if (page == 4) {
      $('.embed-calculator').height('2000px');
    } else if (page == 5) {
      $('.embed-calculator').height('1350px');
    } else if (page == 6) {
      $('.embed-calculator').height('900px');
    }*/
  }
  
   function submitLead(){
	var data = encodeURIComponent(JSON.stringify(customerResult));
	var win=window.open('submit.php?data=' + data + '&update=0', '_blank');
	win.focus();	
  }
  
  function getLeadCookie() {
	  var name = "lead_id=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		  c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		  return c.substring(name.length, c.length);
		}
	  }
	  return "";
	}
    
  function getBudgetCalculator(){	
		
	var id = $.urlParam('ID');
	var app = $.urlParam('App');
	
	$.get("https://applicationform.hatpacks.com.au/api/enquiryform/GetBudgetCalculator", { guid: id, app: app } )
	.done(function( data ) {
		if(data != null) {
			budgetCalculator = data;	
			var cc = 0;			
			if(data.Items != null && data.Items.length > 0) {
				data.Items.forEach(function(item) {					
					if(item.ItemTypeID == 33) {
						cc += 1;
						if(cc > 1)
							addCreditCard();						
					}					
					var $txt = $(document).find("[data-type-id='" + item.ItemTypeID + "']").last(); 
					if($txt != null) {
						$txt.val(item.ItemValue);
						var cbo = $txt.closest('li').find('select').get(0);						
						if(cbo != null) {
							$(cbo).val(item.ItemFrequency);
						}
						var other = $txt.closest('li').find('.other');
						if(item.ItemComments != null && item.ItemComments.length > 0 && other != null) {
							$(other).val(item.ItemComments);
						}
						$($txt).trigger( "change" );
						
						if(item.ItemTypeID == 33 && item.ItemComments.length > 0) {
							var ccDetails = item.ItemComments.split("|");
							var ccFields = $(document).find(".credit-card-details").last().find("input[type=textarea]");
							$(ccFields[0]).val(ccDetails[0]);
							$(ccFields[1]).val(ccDetails[1]);
							$(ccFields[2]).val(ccDetails[2]);
						}
					}
				});
			}
		}
	})
	.fail(function() {});	
  }
  
  function postBudgetCalculator(){	
	//var data = encodeURIComponent(JSON.stringify(finalResult));
	//var win=window.open('submit.php?data=' + data + '&update=1', '_blank');	
	//win.focus();		
	if(budgetCalculator == null && getLeadCookie() != '') {	
		budgetCalculator = { 
							 BudgetCalculatorID: 0, 
							 CallID: getLeadCookie(),
							 TotalExpenses: 0,
							 Items: []
						   };
	}
	
	if(budgetCalculator != null) {	
		var items = [];
		var value  = 0;		
		var totalIncome = 0;
		var totalExpenses = 0;
		var totalSurplusDeficit = 0;
		
		$('[data-type-id]').each(function(i, txt) {					
			value = parseFloat($(txt).val().replace(',', ''));	
			totalIncome = parseFloat($('#income-monthly-value').text().replace(',', ''));
			totalExpenses = parseFloat($('#expenses-monthly-value').text().replace(',', ''));
			totalSurplusDeficit = parseFloat($('#surplus-monthly-value').text().replace(',', ''));	
			if (isNaN(value)) {
				value = 0;
			}		
			var comments = null;			
			var other = $(txt).closest('li').find('.other');
			if(other != null && other.length > 0)
				comments = $(other).val();			
			if($(txt).data('type-id') == 33) {
				var ccFields = $(txt).closest('li').find(".credit-card-details").find("input[type=textarea]");
				comments = $(ccFields[0]).val() + '|' + $(ccFields[1]).val() + '|' + $(ccFields[2]).val();
			}			
			if(value > 0) {
				items.push
				({
					ItemID: 0,
					BudgetCalculatorID: budgetCalculator.BudgetCalculatorID,
					ItemTypeID: $(txt).data('type-id'),
					ItemValue: value,
					ItemFrequency: $($(txt).closest('li').find('select').get(0)).val(),
					ItemComments: comments
				});	
			}
		});	
		budgetCalculator.TotalIncome = totalIncome;
		budgetCalculator.TotalExpenses = totalExpenses;
		budgetCalculator.TotalSurplusDeficit = totalSurplusDeficit;
		budgetCalculator.Items = items;
		
		$.post("https://applicationform.hatpacks.com.au/api/enquiryform/PostBudgetCalculator", JSON.stringify(budgetCalculator) )
		.done(function( data ) {
			if(data != null && data.ID != null && data.ID > 0) {
				budgetCalculator.BudgetCalculatorID = data.ID;
			}
		})
		.fail(function() {});	
	}
  }
  
  function addCreditCard() {
	  var cc = $( '.credit-card-section' ).last().clone();
	  $($(cc).find('label')[0]).text('Credit card #' + ($( '.credit-card-section' ).length + 1) + ':');
	  $(cc).find('input[type=text]').val('');
	  $(cc).find('input[type=textarea]').val('');
	  $(cc).find('.amount').text('0');
	  $('#add-credit-card' ).remove();
	  $(cc).insertAfter($('.credit-card-section').last());
  }
  
  renderTabs();
  
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(document.referrer);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
  }

  $(document).
	ready(function() {
	  if (document.referrer.indexOf("?ID=") > -1) {		
		$('#calc-budget-planner-customer').hide();
		$('#calc-budget-planner-calculator').show();	
		$('.page').show();
	    getBudgetCalculator();
	  }
	}).  
	on('click', '#get-started', function (e) {
      if(!$('#txtFirstName').val() || !$('#txtEmail').val() || !$('#txtMobile').val()) {
		$('#required-fields').show();	
		$('#required-fields').text('Please fill in all the fields!');			
	  } else if($("#txtMobile").val().indexOf("04") != 0 || $("#txtMobile").val().length != 10){
		$('#required-fields').show();	
		$('#required-fields').text('Invalid Mobile number, please try again');
	  } else if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#txtEmail").val()) == false){
		$('#required-fields').show();	
		$('#required-fields').text('Invalid Email, please try again');
	  } else {		
		customerResult.push({key: 'First Name', value: $('#txtFirstName').val()});
		customerResult.push({key: 'Mobile', value: $('#txtMobile').val()});
		customerResult.push({key: 'Email', value: $('#txtEmail').val()});		  
		$('#calc-budget-planner-customer').hide();
		$('#calc-budget-planner-calculator').show();	
		$('.page').show();	
		submitLead();
	  }
    }).
    on('click', '.nav-menu li', function (e) {
      renderTabs(parseInt($(this).data('nav'), 10));
    }).
    on('click', '.prev-tab, .next-tab', function (e) {
      e.preventDefault();
      var $element = $(this);
      var next = currentPage;
      if ($element.hasClass('next-tab')) {
        next += 1;
		if(next == 4) {
			next += 1;
		}
      } else {
        next -= 1;
		if(next == 4) {
			next -= 1;
		}
      }
      if (!(next < 1 || next > 6)) {
        renderTabs(next);
      }
	  postBudgetCalculator();	  
    }).
    on('change', '[data-category] select, [data-category] input[type=text]', onFieldUpdate).
	on('change', '.credit-card-details input[type=textarea]', onCreditCardFieldUpdate).
    on('click', '.calc .email', function (e) {
      e.preventDefault();
      var $form = $('#email-form');
      var isVisible = $form.is(':visible');
      $form[isVisible ? 'hide' : 'show']();
      $(".calc #message").val(''); //.val(isVisible ? '' : LoanCalc.resultToString(finalResult));
    }).
    on('click', '.calc .save', function (e) {
      e.preventDefault();
      var data = encodeURIComponent(JSON.stringify(finalResult));
      var win=window.open('save.php?data=' + data + '&title=Budget Planner', '_blank');
      win.focus();
    }).
	on('click', '#add-credit-card', function () {
	  addCreditCard();
    }).
	on('click', '.calc .nav-menu li', function (e) {
      e.preventDefault();
      postBudgetCalculator();	
    }).
    on('click', '.calc .print', function () {
      window.print();
    }).
    on('click', '#reset-fields', function (e) {
      e.preventDefault();
      $('[data-category] input[type=text]').val('').trigger('change');
	  $('[data-category] input[type=textarea]').val('').trigger('change');
      $('[data-category] select').each(function () {
        this.selectedIndex = 1;
      });
	  //$('#reset-fields-modal').modal('hide');
    })
  ;

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  $('[data-category] select, [data-category] input[type=text]').trigger('change');
})(jQuery);

